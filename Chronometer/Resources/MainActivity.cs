﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Util;

namespace Chronometer
{
	[Activity (Label = "@string/app_name", MainLauncher = true, Icon = "@drawable/chrono")]
	public class MainActivity : Activity
	{
		private ISharedPreferences data;
		//private DateTime start;
		private Handler timer;
		private TimeSpan accumulatedTime = new TimeSpan();
		private DateTime prevTime;
		private bool counting = false;

		private TextView timeView;
		private Button startStopButton;
		private Button resetButton;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			data = this.GetSharedPreferences ("chrono", FileCreationMode.Private);
			timer = new Handler ();

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main); 	
			timeView = (TextView)FindViewById (Resource.Id.timeView);
			startStopButton = (Button)FindViewById (Resource.Id.startStopButton);
			resetButton = (Button)FindViewById (Resource.Id.resetButton);

			startStopButton.Click += (sender, eventArgs) => StartStop();
			resetButton.Click += (sender, eventArgs) => Reset();

			ShowCount ();

			//Log.Info ("bram", "OnCreate");
		}

		private void ShowCount() {
			//string h = (accumulatedTime.TotalHours%100).ToString ("00");
			//string m = (accumulatedTime.TotalMinutes%100).ToString ("00");
			//string s = (accumulatedTime.TotalSeconds%100).ToString ("00");
			//string ms = (accumulatedTime.TotalMilliseconds%1000).ToString ("000");
			timeView.Text = string.Format("{0:hh\\:mm\\:ss\\.fff}", accumulatedTime);
			//timeView.Text = h + ":" + m + ":" + s + "." + ms;
		}



		private void Increment() {
			accumulatedTime = accumulatedTime.Add(DateTime.Now - prevTime);
			prevTime = DateTime.Now;
			ShowCount ();
		}

		private void Reset() {
			if (counting)
				StartStop ();
			accumulatedTime = TimeSpan.Zero;
			ShowCount ();
		}

		protected override void OnResume() {
			base.OnResume ();

			//Log.Info ("bram", "OnResume");

			accumulatedTime = TimeSpan.Parse(data.GetString ("accumulatedTime", "0"));
			string prevTimeString = data.GetString("prevTime", "0");
			if (!prevTimeString.Equals ("0")) {
				prevTime = DateTime.Parse (prevTimeString);
				StartStop (true);
			}
			ShowCount ();
		}
		protected override void OnPause() {
			base.OnPause ();

			//Log.Info ("bram", "OnPause");

			ISharedPreferencesEditor editor = data.Edit();
			editor.PutString ("accumulatedTime", accumulatedTime.ToString());
			if (counting) {
				editor.PutString ("prevTime", prevTime.ToString ());
			} else {
				editor.PutString ("prevTime", "0");
			}
			editor.Commit ();
		}

		private void StartStop(bool cont = false) {
			if (counting) {
				counting = false;
				Increment ();
				ShowCount ();
				startStopButton.Text = Resources.GetString(Resource.String.chrono_start);
			} else {
				counting = true;
				startStopButton.Text = Resources.GetString(Resource.String.chrono_stop);
				if (!cont) {
					prevTime = DateTime.Now;
				}
				GenerateDelayedTick ();
			}
		}

		private void GenerateDelayedTick()
		{
			// Ask timer to call OnTick after 100ms
			// This call returns immediately
			timer.PostDelayed (OnTick, 250);

			//timer.
		}

		private void OnTick()
		{
			if (counting) {
				Increment ();
				GenerateDelayedTick ();
			}
		} 
	}
}


